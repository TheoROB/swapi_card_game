let button = document.getElementById("button");
let title = document.getElementById("title");
let categorie = document.getElementById("categorie");
let gender = document.getElementById("gender");
let height = document.getElementById("height");
let photo = document.getElementById("photo");
let description = document.getElementById("description");
let cats;
let value;

function getCats() {
  let cat = Math.round(Math.random() * 4) + 1;

  switch (cat) {
    case 1:
      cat = "people";
      break;
    case 2:
      cat = "planets";
      break;
    case 3:
      cat = "species";
      break;
    case 4:
      cat = "starships";
      break;
    case 5:
      cat = "vehicules";
      break;
  }
  cats = cat;
}

function randomNumber() {}

function getPhoto() {
  if (cats === "people") {
    value = Math.floor(Math.random() * 83);
    photo.innerHTML = `<img src="../../img/people/${value}.jpg" class="card-img-top" alt="" />`;
  } else if (cats === "planets") {
    value = Math.floor(Math.random() * 60);
    photo.innerHTML = `<img src="../../img/planets/${value}.jpg" class="card-img-top" alt="" />`;
  } else if (cats === "species") {
    value = Math.floor(Math.random() * 37);
    photo.innerHTML = `<img src="../../img/species/${value}.jpg" class="card-img-top" alt="" />`;
  } else if (cats === "starships") {
    value = Math.floor(Math.random() * 36);
    photo.innerHTML = `<img src="../../img/starships/${value}.jpg" class="card-img-top" alt="" />`;
  } else if (cats === "vehicules") {
    value = Math.floor(Math.random() * 39);
    photo.innerHTML = `<img src="../../img/vehicles/${value}.jpg" class="card-img-top" alt="" />`;
  }
}

async function recuperer() {
  getCats();
  getPhoto();
  const res = await fetch("https://swapi.dev/api/" + cats + "/" + `${value}`);
  const data = await res.json();
  console.log(data);
  categorie.innerText = cats;
  title.innerText = data.name;
  gender.innerText = data.gender;
  height.innerText = data.height;
}

button.addEventListener("click", recuperer);
